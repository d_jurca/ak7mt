package com.utb.jurca.util

object Constants {
    const val BEARER_TOKEN = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4N2NjMzQ4YzI5MmQ2MGVmYTQyZjQ5OWY0YWU1MjVjZCIsInN1YiI6IjY1YWZiMTAxMTU4Yzg1MDBlYmJhMjRkMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.bf3PHjZ2Xk2bfLCIS4LBLsinLCyc6iKZfDJjKVa7za8"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/"
    const val MOVIE_DB_NAME = "movies_database"
}