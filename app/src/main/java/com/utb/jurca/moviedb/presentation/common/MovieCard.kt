package com.utb.jurca.moviedb.presentation.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.utb.jurca.moviedb.data.remote.responses.Movie
import com.utb.jurca.moviedb.presentation.Dimensions.MovieCardSize
import com.utb.jurca.util.Constants.IMAGE_BASE_URL
import kotlin.math.roundToInt

@Composable
fun MovieCard(
    movie: Movie,
    navController: NavController,
    modifier: Modifier = Modifier,
    isFavorite: Boolean = false
) {

    val context = LocalContext.current

    Row(
        modifier = modifier
            .padding(vertical = 8.dp)
            .clickable {
                navController.navigate("movieDetailsScreen/${movie.id}/${isFavorite}")
            }
            .padding(start = 15.dp, end = 10.dp, top = 5.dp, bottom = 5.dp)
    ) {

        AsyncImage(
            modifier = Modifier
                .size(MovieCardSize)
                .clip(MaterialTheme.shapes.medium),
            model = ImageRequest.Builder(context).data(IMAGE_BASE_URL + "w185" + movie.poster_path).build(),
            contentDescription = movie.title,
            contentScale = ContentScale.Crop
        )

        Column(
            verticalArrangement = Arrangement.SpaceAround,
            modifier = Modifier
                .padding(horizontal = 10.dp)
                .height(MovieCardSize)
        ) {
            Text(
                text = movie.title + " (" + movie.release_date.substring(0, 4) + ")",
                style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )

            Text(
                text = movie.overview,
                style = MaterialTheme.typography.bodySmall,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = (movie.vote_average*10).roundToInt().toString() + "% ",
                    style = MaterialTheme.typography.bodySmall.copy(fontWeight = FontWeight.Bold),
                    maxLines = 1
                )

                Text(
                    text = "(" + movie.vote_count.toString() + " votes)",
                    style = MaterialTheme.typography.bodySmall,
                    maxLines = 1
                )
            }
        }

    }
}
