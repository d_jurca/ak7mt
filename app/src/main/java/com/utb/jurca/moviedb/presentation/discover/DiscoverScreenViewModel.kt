package com.utb.jurca.moviedb.presentation.discover

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.utb.jurca.moviedb.data.remote.repository.MoviesRepository
import com.utb.jurca.moviedb.data.remote.responses.Movie
import com.utb.jurca.util.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DiscoverScreenViewModel @Inject constructor(
    private val repository : MoviesRepository
) : ViewModel() {

    private var page = 1
    var movieList = mutableStateOf<List<Movie>>(listOf())
    var isLoading = mutableStateOf(false)
    var error = mutableStateOf("")
    var endReached = mutableStateOf(false)

    init {
        loadMoviesPaginated()
    }

    fun loadMoviesPaginated() {
        viewModelScope.launch {
            isLoading.value = true

            val result = repository.getMovieList(page)
            when (result) {
                is ApiResult.Success -> {
                    endReached.value = page >= result.data!!.total_pages
                    val movies = result.data.results

                    page++
                    error.value = ""
                    isLoading.value = false
                    movieList.value += movies
                }
                is ApiResult.Error -> {
                    error.value = result.error!!
                }
                is ApiResult.Loading -> {
                    isLoading.value = true
                }
            }
        }
    }

}