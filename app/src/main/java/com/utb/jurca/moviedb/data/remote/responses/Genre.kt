package com.utb.jurca.moviedb.data.remote.responses

data class Genre(
    val id: Int,
    val name: String
)