package com.utb.jurca.moviedb.presentation.common

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.utb.jurca.moviedb.presentation.discover.DiscoverScreenViewModel

@Composable
fun MovieList(
    navController: NavController,
    viewModel: DiscoverScreenViewModel
) {

    val movies by remember { viewModel.movieList }
    val isLoading by remember { viewModel.isLoading }
    val endReached by remember { viewModel.endReached }
    val error by remember { viewModel.error }

    Log.d("MovieList composable", "Movies: ${movies.count()}")

    LazyColumn() {
        items(movies.count()) {
            if (it >= movies.count() - 1 && !endReached && !isLoading) {
                viewModel.loadMoviesPaginated()
            }

            MovieCard(movie = movies[it], navController = navController)
        }
    }

    Box(
        contentAlignment = Center,
        modifier = Modifier.fillMaxSize()
    ) {
        if(isLoading) {
            CircularProgressIndicator(color = MaterialTheme.colorScheme.primary)
        }
        if(error.isNotEmpty()) {
            RetrySection(error = error) {
                viewModel.loadMoviesPaginated()
            }
        }
    }

}

@Composable
fun RetrySection(
    error: String,
    onRetry: () -> Unit
) {
    Column {
        Text(error, color = Color.Red, fontSize = 18.sp)
        Spacer(modifier = Modifier.height(8.dp))
        Button(
            onClick = { onRetry() },
            modifier = Modifier.align(CenterHorizontally)
        ) {
            Text(text = "Retry")
        }
    }
}

/*
@Composable
fun handlePagingResult(
    movies: LazyPagingItems<Movie>
): Boolean {

    val loadState = movies.loadState
    val error = when {
        loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
        else -> null
    }

    return when {
        error != null -> {
            false
        }
        else -> true
    }
}*/
