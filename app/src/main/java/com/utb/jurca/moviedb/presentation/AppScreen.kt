package com.utb.jurca.moviedb.presentation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.ui.graphics.vector.ImageVector

sealed class AppScreen(
    val route: String,
    val title: String,
    val icon: ImageVector
) {
    object DiscoverScreen : AppScreen(
        route = "discoverScreen",
        title = "Discover",
        icon = Icons.Default.Home
    )

    object MovieDetailScreen : AppScreen(
        route = "movieDetailsScreen/{movieId}/{isFavorite}",
        title = "Detail",
        icon = Icons.Default.Info
    )

    object FavoritesScreen : AppScreen(
        route = "favoritesScreen",
        title = "Favorites",
        icon = Icons.Default.Favorite
    )
}