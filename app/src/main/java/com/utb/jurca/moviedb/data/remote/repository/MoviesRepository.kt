package com.utb.jurca.moviedb.data.remote.repository

import android.util.Log
import com.utb.jurca.moviedb.data.remote.MovieApi
import com.utb.jurca.moviedb.data.remote.responses.MovieDetail
import com.utb.jurca.moviedb.data.remote.responses.MovieList
import com.utb.jurca.util.ApiResult
import com.utb.jurca.util.Constants.BEARER_TOKEN
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class MoviesRepository @Inject constructor(private val api: MovieApi) {

    suspend fun getMovieList(page: Int): ApiResult<MovieList> {
        val response = try {
            api.getMovieList(BEARER_TOKEN, page)
        } catch (e: Exception) {
            Log.e("MoviesRepository", "Error: ${e.message}")
            return ApiResult.Error(e.message ?: "Unknown error")
        }

        Log.d("MoviesRepository", "Response: $response")
        return ApiResult.Success(response)
    }

    suspend fun getMovieDetail(movieId: Int): ApiResult<MovieDetail> {
        Log.d("MoviesRepository", "Movie ID: $movieId")
        val response = try {
            api.getMovieDetail(BEARER_TOKEN, movieId)
        } catch (e: Exception) {
            Log.e("MoviesRepository", "Error: ${e.message}")
            return ApiResult.Error(e.message ?: "Unknown error")
        }

        Log.d("MoviesRepository", "Response: $response")
        return ApiResult.Success(response)
    }

}