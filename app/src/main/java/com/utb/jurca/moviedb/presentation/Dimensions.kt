package com.utb.jurca.moviedb.presentation

import androidx.compose.ui.unit.dp

object Dimensions {

    val MovieCardSize = 120.dp

}