package com.utb.jurca.moviedb.presentation.favorites

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.utb.jurca.moviedb.presentation.common.MovieCard

@Composable
fun FavoritesScreen(
    navController: NavController,
    viewModel: FavoritesViewModel
) {
    val movies by remember { viewModel.movieList }

    Surface(
        color = MaterialTheme.colorScheme.background
    ) {
        if (movies.isNotEmpty()) {
            Column {
                movies.forEach() {
                    MovieCard(movie = it, navController = navController, isFavorite = true)
                }
            }
        } else {
            Text(
                text = "No favorite movies",
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth().padding(top = 30.dp)
            )
        }
    }
}