package com.utb.jurca.moviedb.presentation.detail

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.AssistChip
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.produceState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.utb.jurca.moviedb.data.models.MovieListItem
import com.utb.jurca.moviedb.data.remote.responses.MovieDetail
import com.utb.jurca.moviedb.presentation.favorites.FavoritesViewModel
import com.utb.jurca.util.ApiResult
import com.utb.jurca.util.Constants
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

@Composable
fun MovieDetailScreen(
    movieId: Int,
    isFavorite: Boolean,
    navController: NavController,
    viewModel: MovieDetailViewModel,
    favoritesViewModel: FavoritesViewModel
) {

    val movie = produceState<ApiResult<MovieDetail>>(initialValue = ApiResult.Loading()) {
        value = viewModel.getMovieDetail(movieId)
    }.value

    Column {
        MovieDetailTopSection(
            movie = movie,
            navController = navController,
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.2f),
            viewModel = viewModel,
            isFavorite = isFavorite,
            favoritesViewModel = favoritesViewModel
        )

        if (movie is ApiResult.Success) {
            Column(Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = movie.data!!.original_title,
                    style = MaterialTheme.typography.headlineMedium.copy(fontWeight = FontWeight.Bold),
                    maxLines = 3,
                    modifier = Modifier.padding(top = 16.dp, bottom = 4.dp, start = 16.dp, end = 16.dp),
                    textAlign = TextAlign.Center
                )

                Text(
                    text = "(" + movie.data.release_date.substring(0, 4) + ")",
                    style = MaterialTheme.typography.headlineSmall,
                    maxLines = 3,
                    textAlign = TextAlign.Center
                )
            }

            Column(modifier = Modifier.padding(all = 16.dp)) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 30.dp),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    Row {
                        Icon(
                            imageVector = Icons.Default.Info,
                            contentDescription = "Runtime",
                            modifier = Modifier
                                .size(16.dp)
                                .padding(end = 4.dp),
                        )

                        Text(
                            text = movie.data!!.runtime.toString() + " min",
                            style = MaterialTheme.typography.bodySmall,
                        )
                    }

                    Row {
                        Icon(
                            imageVector = Icons.Default.DateRange,
                            contentDescription = "Released",
                            modifier = Modifier
                                .size(16.dp)
                                .padding(end = 4.dp),
                        )

                        Text(
                            text = movie.data!!.release_date,
                            style = MaterialTheme.typography.bodySmall,
                        )
                    }
                }

                Text(
                    text = "Overview",
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier.padding(bottom = 8.dp)
                )

                Text(
                    text = movie.data!!.overview,
                    style = MaterialTheme.typography.bodySmall,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.padding(horizontal = 4.dp)
                )

                Text(
                    text = "Rating",
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier.padding(top = 24.dp,bottom = 8.dp)
                )

                LinearProgressIndicator(
                    progress = movie.data.vote_average.toFloat()/10,
                    modifier = Modifier
                        .height(10.dp)
                        .fillMaxWidth()
                        .padding(start = 4.dp, end = 4.dp, bottom = 4.dp)
                )

                Text(
                    text = (movie.data.vote_average * 10).roundToInt().toString() + " / 100 (" + movie.data.vote_count.toString() + " votes)",
                    style = MaterialTheme.typography.bodySmall,
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.padding(horizontal = 4.dp)
                )

                Text(
                    text = "Genres",
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier.padding(top = 24.dp,bottom = 4.dp)
                )

                Row {
                    var i = 0
                    movie.data.genres.forEach {
                        if (i < 3) {
                            AssistChip(
                                onClick = { },
                                label = { Text(it.name) },
                                modifier = Modifier.padding(horizontal = 4.dp)
                            )
                            i++
                        }
                    }
                }
            }
        } else {
            Text(
                text = "Movie not found",
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth().padding(top = 30.dp)
            )
        }
    }
}

@Composable
fun MovieDetailTopSection(
    movie: ApiResult<MovieDetail>,
    navController: NavController,
    modifier: Modifier = Modifier,
    viewModel: MovieDetailViewModel,
    isFavorite: Boolean,
    favoritesViewModel: FavoritesViewModel
) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    Box(
        contentAlignment = Alignment.TopStart,
        modifier = modifier
            .background(color = MaterialTheme.colorScheme.background)
    ) {
        if (movie is ApiResult.Success) {
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth(),
                model = ImageRequest.Builder(context).data(Constants.IMAGE_BASE_URL + "w780" + movie.data!!.backdrop_path).build(),
                contentDescription = movie.data.title,
                contentScale = ContentScale.FillWidth
            )

            Box(modifier = modifier
                .matchParentSize()
                .background(
                    Brush.verticalGradient(
                        listOf(
                            Color.Black,
                            Color.Transparent,
                            Color.Transparent
                        )
                    )
                )
            )
        }

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = "Back",
                tint = Color.White,
                modifier = Modifier
                    .size(36.dp)
                    .clickable {
                        navController.popBackStack()
                    }
            )

            if (isFavorite) {
                Icon(
                    imageVector = Icons.Default.Delete,
                    contentDescription = "Favorite",
                    tint = Color.White,
                    modifier = Modifier
                        .size(36.dp)
                        .clickable {
                            coroutineScope.launch {
                                viewModel.deleteMovie(
                                    MovieListItem(
                                        id = movie.data!!.id,
                                        title = movie.data.title,
                                        poster_path = movie.data.poster_path,
                                        overview = movie.data.overview,
                                        vote_average = movie.data.vote_average,
                                        vote_count = movie.data.vote_count,
                                        release_date = movie.data.release_date
                                    )
                                )
                                favoritesViewModel.getFavoriteMovies()
                            }
                            navController.popBackStack()
                            Toast
                                .makeText(context, "Deleted from favorites", Toast.LENGTH_SHORT)
                                .show()
                        }
                )
            } else {
                Icon(
                    imageVector = Icons.Default.Favorite,
                    contentDescription = "Favorite",
                    tint = Color.White,
                    modifier = Modifier
                        .size(36.dp)
                        .clickable {
                            coroutineScope.launch {
                                viewModel.upsertMovie(
                                    MovieListItem(
                                        id = movie.data!!.id,
                                        title = movie.data.title,
                                        poster_path = movie.data.poster_path,
                                        overview = movie.data.overview,
                                        vote_average = movie.data.vote_average,
                                        vote_count = movie.data.vote_count,
                                        release_date = movie.data.release_date
                                    )
                                )
                                favoritesViewModel.getFavoriteMovies()
                            }
                            Toast
                                .makeText(context, "Added to favorites", Toast.LENGTH_SHORT)
                                .show()
                        }
                )
            }
        }
    }
}