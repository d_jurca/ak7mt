package com.utb.jurca.moviedb.di

import android.app.Application
import androidx.room.Room
import com.utb.jurca.moviedb.data.local.MovieDatabase
import com.utb.jurca.moviedb.data.remote.MovieApi
import com.utb.jurca.moviedb.data.remote.repository.MoviesRepository
import com.utb.jurca.util.Constants.BASE_URL
import com.utb.jurca.util.Constants.MOVIE_DB_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides @Singleton
    fun provideMovieRepository(api: MovieApi) = MoviesRepository(api)

    @Provides @Singleton
    fun provideMovieApi(): MovieApi {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(MovieApi::class.java)
    }

    @Provides @Singleton
    fun provideMovieDatabase(
        app: Application
    ): MovieDatabase {
        return Room.databaseBuilder(
            context = app,
            klass = MovieDatabase::class.java,
            name = MOVIE_DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides @Singleton
    fun provideMovieDao(db: MovieDatabase) = db.moviesDao()

}