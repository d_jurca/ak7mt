package com.utb.jurca.moviedb

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.remember
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.utb.jurca.moviedb.data.local.MovieDatabase
import com.utb.jurca.moviedb.presentation.AppScreen
import com.utb.jurca.moviedb.presentation.common.BottomBar
import com.utb.jurca.moviedb.presentation.detail.MovieDetailScreen
import com.utb.jurca.moviedb.presentation.detail.MovieDetailViewModel
import com.utb.jurca.moviedb.presentation.discover.DiscoverScreen
import com.utb.jurca.moviedb.presentation.discover.DiscoverScreenViewModel
import com.utb.jurca.moviedb.presentation.favorites.FavoritesScreen
import com.utb.jurca.moviedb.presentation.favorites.FavoritesViewModel
import com.utb.jurca.moviedb.ui.theme.MovieDBTheme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val discoverScreenViewModel by viewModels<DiscoverScreenViewModel>()
    private val movieDetailViewModel by viewModels<MovieDetailViewModel>()
    private val favoritesScreenViewModel by viewModels<FavoritesViewModel>()

    @Inject
    lateinit var dao: MovieDatabase

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()

        setContent {
            MovieDBTheme {
                val navController = rememberNavController()

                Scaffold(
                    bottomBar = { BottomBar(navController) }
                ) {
                    NavHost(navController = navController, startDestination = AppScreen.DiscoverScreen.route) {
                        composable(AppScreen.DiscoverScreen.route) {
                            DiscoverScreen(navController, discoverScreenViewModel)
                        }

                        composable(
                            AppScreen.MovieDetailScreen.route,
                            arguments = listOf(
                                navArgument("movieId") { type = NavType.IntType },
                                navArgument("isFavorite") { type = NavType.BoolType }
                            )
                        ) {
                            val movieId = remember {
                                it.arguments?.getInt("movieId")
                            }

                            val favorite = remember {
                                it.arguments?.getBoolean("isFavorite")
                            }

                            MovieDetailScreen(
                                movieId!!,
                                favorite!!,
                                navController,
                                movieDetailViewModel,
                                favoritesScreenViewModel
                            )
                        }

                        composable(AppScreen.FavoritesScreen.route) {
                            FavoritesScreen(navController, favoritesScreenViewModel)
                        }
                    }
                }
            }
        }
    }
}