package com.utb.jurca.moviedb.presentation.detail

import androidx.lifecycle.ViewModel
import com.utb.jurca.moviedb.data.local.MoviesDao
import com.utb.jurca.moviedb.data.models.MovieListItem
import com.utb.jurca.moviedb.data.remote.repository.MoviesRepository
import com.utb.jurca.moviedb.data.remote.responses.MovieDetail
import com.utb.jurca.util.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val repository: MoviesRepository,
    private val moviesDao: MoviesDao
) : ViewModel() {
    suspend fun getMovieDetail(movieId: Int): ApiResult<MovieDetail> {
        return repository.getMovieDetail(movieId)
    }

    suspend fun upsertMovie(movie: MovieListItem) {
        moviesDao.upsert(movie)
    }

    suspend fun deleteMovie(movie: MovieListItem) {
        moviesDao.delete(movie)
    }
}