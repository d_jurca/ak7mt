package com.utb.jurca.moviedb.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieListItem(
    @PrimaryKey val id: Int,
    val overview: String,
    val poster_path: String,
    val title: String,
    val vote_average: Double,
    val vote_count: Int,
    val release_date: String
) {
}