package com.utb.jurca.moviedb.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.utb.jurca.moviedb.data.models.MovieListItem

@Database(entities = [MovieListItem::class], version = 2)
abstract class MovieDatabase: RoomDatabase() {

    abstract fun moviesDao(): MoviesDao

}