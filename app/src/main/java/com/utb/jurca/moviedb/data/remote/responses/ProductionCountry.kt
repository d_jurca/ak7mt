package com.utb.jurca.moviedb.data.remote.responses

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)