package com.utb.jurca.moviedb.data.remote

import com.utb.jurca.moviedb.data.remote.responses.MovieDetail
import com.utb.jurca.moviedb.data.remote.responses.MovieList
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("movie/popular")
    suspend fun getMovieList(
        @Header("Authorization") bearer: String,
        @Query("page") page: Int
    ): MovieList

    @GET("movie/{movieId}")
    suspend fun getMovieDetail(
        @Header("Authorization") bearer: String,
        @Path("movieId") movieId: Int
    ): MovieDetail
}