package com.utb.jurca.moviedb.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.utb.jurca.moviedb.data.models.MovieListItem

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun upsert(movie: MovieListItem)

    @Delete
    suspend fun delete(movie: MovieListItem)

    @Query("SELECT * FROM MovieListItem")
    suspend fun getMovies(): List<MovieListItem>

}