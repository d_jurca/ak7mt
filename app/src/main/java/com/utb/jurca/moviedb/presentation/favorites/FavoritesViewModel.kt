package com.utb.jurca.moviedb.presentation.favorites

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.utb.jurca.moviedb.data.local.MoviesDao
import com.utb.jurca.moviedb.data.remote.responses.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val moviesDao: MoviesDao
): ViewModel() {

    var movieList = mutableStateOf<List<Movie>>(listOf())

    init {
        getFavoriteMovies()
    }

    fun getFavoriteMovies() {
        viewModelScope.launch {
            movieList.value = listOf()
            val movies = moviesDao.getMovies()

            movies.forEach() {
                movieList.value += Movie(
                    id = it.id,
                    adult = false,
                    backdrop_path = "",
                    original_language = "",
                    original_title = "",
                    overview = it.overview,
                    popularity = 0.0,
                    poster_path = it.poster_path,
                    release_date = it.release_date,
                    title = it.title,
                    video = false,
                    vote_average = it.vote_average,
                    vote_count = it.vote_count,
                    genre_ids = listOf()
                )
            }
        }
    }
}