package com.utb.jurca.moviedb.presentation.discover

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.utb.jurca.moviedb.presentation.common.MovieList

@Composable
fun DiscoverScreen(
    navController: NavController,
    viewModel: DiscoverScreenViewModel
) {

    Surface(
        color = MaterialTheme.colorScheme.background
    ) {
        Column {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = MaterialTheme.colorScheme.primary)
                    .padding(all = 16.dp),
                text = "The Movie Database",
                style = MaterialTheme.typography.headlineMedium.copy(fontWeight = FontWeight.ExtraBold),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )

            MovieList(navController = navController, viewModel = viewModel)
        }
    }

}